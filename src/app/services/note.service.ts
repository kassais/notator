import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Post } from "../models/post";

@Injectable({
  providedIn: 'root'
})
export class NoteService {

  posts: Post[] = [];

  constructor() { }

  addPost(post){
    if( this.posts && this.posts.length){
      post.id = this.posts.length + 1;
      this.posts.push(post);
      console.log(this.posts);
    } else{
      this.posts.push(post);
    }
  }

  getPosts(): Observable<Post[]>{
    return of(this.posts);
  }

  getPost(id): Observable<Post> {
    return of(this.posts.find(post => post.id === id) );
  }
}
