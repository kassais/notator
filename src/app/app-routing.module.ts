import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostViewComponent } from "./components/post-view/post-view.component";
import { NewPostComponent } from "./components/new-post/new-post.component";

const routes: Routes = [
  {path: '', component: NewPostComponent},
  {path: 'post/:id', component: PostViewComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
