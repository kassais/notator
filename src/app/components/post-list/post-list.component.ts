import { Component, OnInit } from '@angular/core';
import { NoteService } from "../../services/note.service";
import { Post } from "../../models/post";

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {

  posts: Post[];

  constructor(
    public noteService: NoteService
  ) { }

  ngOnInit() {
    this.getPosts();
  }

  getPosts(){
    this.noteService.getPosts()
      .subscribe(posts => this.posts = posts);
  }
}
