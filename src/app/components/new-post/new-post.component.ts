import { Component, OnInit } from '@angular/core';
import { NoteService } from "../../services/note.service";
import { Post } from "../../models/post";

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.scss']
})
export class NewPostComponent implements OnInit {

  post: Post = {
    id: 1,
    title: '',
    content: ''
  };

  constructor(
    public noteService: NoteService
  ) { }

  ngOnInit() {
  }

  addPost(){
    this.noteService.addPost(this.post);
    this.post = {
      id: 1,
      title: '',
      content: ''
    };
  }

}
