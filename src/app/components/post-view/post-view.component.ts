import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NoteService } from "../../services/note.service";
import { Post } from "../../models/post";

@Component({
  selector: 'app-post-view',
  templateUrl: './post-view.component.html',
  styleUrls: ['./post-view.component.scss']
})
export class PostViewComponent implements OnInit {

  post: Post;

  constructor(
    public route: ActivatedRoute,
    public noteService: NoteService
  ) { }

  ngOnInit(): void {
    this.getPost();
  }

  getPost(): void{
    const id = +this.route.snapshot.paramMap.get('id');

    this.noteService.getPost(id)
      .subscribe(post => this.post = post);
  }

}
